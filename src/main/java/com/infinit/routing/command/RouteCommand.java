package com.infinit.routing.command;

import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Command object passed to the RouteController in order to find routes between two ports
 *
 * Created by owahlen on 26.04.15.
 */
public class RouteCommand {

	@NotEmpty
	@ApiModelProperty("name of the start port")
	private String startPort;

	@NotEmpty
	@ApiModelProperty("name of the end port")
	private String endPort;

	@ApiModelProperty("maximum search depth")
	private Integer maxRouteLength;

	public String getStartPort() {
		return startPort;
	}

	public void setStartPort(String startPort) {
		this.startPort = startPort;
	}

	public String getEndPort() {
		return endPort;
	}

	public void setEndPort(String endPort) {
		this.endPort = endPort;
	}

	public Integer getMaxRouteLength() {
		return maxRouteLength;
	}

	public void setMaxRouteLength(Integer maxRouteLength) {
		this.maxRouteLength = maxRouteLength;
	}
}
