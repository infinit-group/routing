package com.infinit.routing.config;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.impl.util.FileUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.File;

/**
 * Application configuration for the routing service.
 * The service runs in an embeddes servlet container.
 *
 * Created by owahlen on 26.04.15.
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan("com.infinit.routing")
public class Application implements EmbeddedServletContainerCustomizer {

	public static final String API_PREFIX="/api/v1";

	@Override
	public void customize(ConfigurableEmbeddedServletContainer container) {
	}

	@Bean
	public GraphDatabaseService graphDatabaseService() {
		return new GraphDatabaseFactory().newEmbeddedDatabase("routing.db");
	}

	public static void main(String[] args) throws Exception {
		FileUtils.deleteRecursively(new File("routing.db"));
		SpringApplication app = new SpringApplication(Application.class);
		app.run(args);
	}

}