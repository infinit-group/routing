package com.infinit.routing.config;

import com.infinit.routing.dto.LegDto;
import com.infinit.routing.entity.Leg;
import com.infinit.routing.entity.Port;
import com.infinit.routing.repository.PortRepository;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.neo4j.core.GraphDatabase;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * The development fixtures are used to setup dummy data that can be used for development and testing.
 * <p>
 * Created by owahlen on 26.04.15.
 */
@Profile({"default", "development", "test"})
@Transactional
@Component
public class DevelopmentFixtures implements ApplicationListener<ContextRefreshedEvent>, Ordered {

    private final static String OCEAN_EDGES = "ocean_edges.csv";

    @Autowired
    private PortRepository portRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (event.getApplicationContext().getParent() == null) {
            configure();
        }
    }

    @Override
    public int getOrder() {
        return 20;
    }

    private void configure() {
        ensurePortsAndLegsAvailable();
    }

    private void ensurePortsAndLegsAvailable() {
        List<LegDto> legDtos = getOceanEdges();
        for (LegDto legDto : legDtos) {
            Port startPort = ensurePortAvailable(legDto.getStartPort());
            Port endPort = ensurePortAvailable(legDto.getEndPort());
            if (findLegByName(startPort.getLegs(), legDto.getName()) == null) {
                startPort.addLeg(legDto.getName(), endPort);
                portRepository.save(startPort);
            }
        }
    }

    private Port ensurePortAvailable(String portName) {
        Port port = portRepository.findByName(portName);
        if (port == null) {
            port = portRepository.save(new Port(portName));
        }
        return port;
    }

    private Leg findLegByName(Collection<Leg> legs, String legName) {
        for (Leg leg : legs) {
            if (Objects.equals(leg.getName(), legName)) {
                return leg;
            }
        }
        return null;
    }

    private List<LegDto> getOceanEdges() {
        List<LegDto> oceanEdges = new ArrayList<LegDto>();
        ClassPathResource resource = new ClassPathResource(OCEAN_EDGES);
        InputStream inputStream;
        try {
            inputStream = resource.getInputStream();
        } catch (IOException e) {
            throw new RuntimeException("Unable to read " + OCEAN_EDGES, e);
        }
        Scanner scanner = new Scanner(inputStream, "UTF-8");
        boolean isFirstLine = true;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if(isFirstLine) {
                // skip first line
                isFirstLine=false;
                continue;
            }
            String [] lineParts = line.split(";");
            oceanEdges.add(new LegDto(lineParts[1], lineParts[2]));
        }
        return oceanEdges;
    }

}
