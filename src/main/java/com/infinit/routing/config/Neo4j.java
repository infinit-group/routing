package com.infinit.routing.config;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.config.Neo4jConfiguration;

/**
 * Configuration of the Neo4j database.
 *
 * Created by owahlen on 26.04.15.
 */
@Configuration
@EnableNeo4jRepositories(basePackages = "com.infinit.routing.repository")
public class Neo4j extends Neo4jConfiguration {

	public Neo4j() {
		// define where to scan for entities
		setBasePackage("com.infinit.routing.entity");
	}

}
