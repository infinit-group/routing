package com.infinit.routing.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;

/**
 * Configuration of Spring Security
 * Created by owahlen on 05.01.14.
 */
@Configuration
@EnableWebMvcSecurity
public class Security extends WebSecurityConfigurerAdapter {
    /**
     * Configure HttpSecurity
     *
     * @param http HttpSecurity instance
     * @throws Exception
     */
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .anyRequest().permitAll(); // all other URLs are allowed for anyone (e.g. static content)
        http
                .headers().frameOptions().disable(); // H2 Console uses frames
        http
                .csrf().disable();
    }

}
