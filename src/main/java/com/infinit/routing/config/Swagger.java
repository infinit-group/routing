package com.infinit.routing.config;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.models.dto.ApiInfo;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Configuration of the Swagger REST API documentation generator
 * Created by owahlen on 30.12.14.
 */
// Swagger requires servlet classes that are not available when running tests
@Profile("!test")
@Configuration
@EnableSwagger
public class Swagger {

    private SpringSwaggerConfig springSwaggerConfig;

    /**
     * Required to autowire SpringSwaggerConfig
     */
    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    public void setSpringSwaggerConfig(SpringSwaggerConfig springSwaggerConfig) {
        this.springSwaggerConfig = springSwaggerConfig;
    }

    /**
     * Every SwaggerSpringMvcPlugin bean is picked up by the swagger-mvc framework - allowing for multiple
     * swagger groups i.e. same code base multiple swagger resource listings.
     */
    @Bean
    public SwaggerSpringMvcPlugin customImplementation() {
        return new SwaggerSpringMvcPlugin(this.springSwaggerConfig)
                .apiInfo(apiInfo())
                .includePatterns(Application.API_PREFIX + "/.*");
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Routing REST API",
                "Client API to the Routing Server",
                "http://www.infinit-group.de",
                "info@infinit-group.de",
                "All rights reserved",
                "http://www.infinit-group.de"
        );
    }

}
