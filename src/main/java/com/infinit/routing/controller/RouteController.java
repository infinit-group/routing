package com.infinit.routing.controller;

import com.infinit.routing.command.RouteCommand;
import com.infinit.routing.config.Application;
import com.infinit.routing.dto.RouteDto;
import com.infinit.routing.service.RouteService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;

/**
 * Retrieve all routes between two ports
 * Created by owahlen on 26.04.15.
 */
@RestController
@RequestMapping(Application.API_PREFIX + "/route")
@Api(value = "", description = "Find routes between two ports")
public class RouteController {

	@Autowired
	private RouteService routeService;

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Find ALL routes between two ports",
			notes = "Ports are specified by their names")
	Collection<RouteDto> getRoutes(@Valid
	                               @RequestBody
	                               RouteCommand command) {
		return routeService.findRouteDtos(command);
	}

}
