package com.infinit.routing.dto;

import com.infinit.routing.entity.Leg;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Dto for a Leg
 * Created by owahlen on 26.04.15.
 */
@ApiModel(value = "", description = "DTO of a Leg")
public class LegDto {

	@ApiModelProperty(value = "name of the leg", required = true)
	private String name;

	@ApiModelProperty(value = "start port of the leg", required = true)
	private String startPort;

	@ApiModelProperty(value = "end port of the leg", required = true)
	private String endPort;

	public LegDto(Leg leg) {
		this(leg.getName(), leg.getStartPort().getName(), leg.getEndPort().getName());
	}

	public LegDto(String startPort, String endPort) {
		this(startPort + " -> " + endPort, startPort, endPort);
	}

	public LegDto(String name, String startPort, String endPort) {
		this.name = name;
		this.startPort = startPort;
		this.endPort = endPort;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStartPort() {
		return startPort;
	}

	public void setStartPort(String startPort) {
		this.startPort = startPort;
	}

	public String getEndPort() {
		return endPort;
	}

	public void setEndPort(String endPort) {
		this.endPort = endPort;
	}

	@Override
	public String toString() {
		return startPort + " -> " + endPort;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		LegDto legDto = (LegDto) o;
		return Objects.equals(startPort, legDto.startPort) &&
				Objects.equals(endPort, legDto.endPort);
	}

	@Override
	public int hashCode() {
		return Objects.hash(startPort, endPort);
	}
}
