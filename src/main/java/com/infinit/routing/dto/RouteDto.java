package com.infinit.routing.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infinit.routing.entity.Leg;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Dto for a route
 * Created by owahlen on 26.04.15.
 */
@ApiModel(value = "", description = "DTO of a Route")
public class RouteDto extends ArrayList<LegDto> {

	@ApiModelProperty(value = "legs of the route", required = true)
	@JsonProperty("legs")
	private List<LegDto> legDtos;

	public RouteDto(LegDto... legDtos) {
		super(Arrays.asList(legDtos));
	}

	public RouteDto(List<Leg> legs) {
		super();
		for (Leg leg : legs) {
			LegDto legDto = new LegDto(leg);
			this.add(legDto);
		}
	}

}
