package com.infinit.routing.entity;

import org.springframework.data.neo4j.annotation.*;

import java.util.Objects;

/**
 * Leg between two ports
 * Created by owahlen on 26.04.15.
 */
@RelationshipEntity(type = "LEGS")
public class Leg {

	@GraphId
	Long id;

	private String name;

	@StartNode
	private Port startPort;

	@Fetch
	@EndNode
	private Port endPort;

	public Leg() {
		this(null, null, null);
	}

	public Leg(String name, Port startPort, Port endPort) {
		this.name = name;
		this.startPort = startPort;
		this.endPort = endPort;
	}

	public Port getStartPort() {
		return startPort;
	}

	public void setStartPort(Port startPort) {
		this.startPort = startPort;
	}

	public Port getEndPort() {
		return endPort;
	}

	public void setEndPort(Port endPort) {
		this.endPort = endPort;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return startPort + " -> "+endPort;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) return true;

		if (id == null) return false;

		if (! (other instanceof Leg)) return false;

		return id.equals(((Leg) other).id);
	}

	@Override
	public int hashCode() {
		return id == null ? System.identityHashCode(this) : id.hashCode();
	}
}
