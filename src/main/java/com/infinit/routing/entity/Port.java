package com.infinit.routing.entity;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.*;

@NodeEntity
public class Port {

	@GraphId
	Long id;

	@Indexed(unique = true)
	private String name;

	@Fetch
	@RelatedToVia(type = "LEGS", direction = Direction.OUTGOING)
	Set<Leg> legs = new HashSet<Leg>();

	public Port() {
	}

	public Port(String name) {
		this.name = name;
	}

	public void addLeg(String legName, Port destinationPort) {
		Leg leg = new Leg(legName, this, destinationPort);
		this.legs.add(leg);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Leg> getLegs() {
		return legs;
	}

	@Override
	public String toString() {
		return "Port: " + name;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) return true;

		if (id == null) return false;

		if (! (other instanceof Port)) return false;

		return id.equals(((Port) other).id);
	}

	@Override
	public int hashCode() {
		return id == null ? System.identityHashCode(this) : id.hashCode();
	}
}
