package com.infinit.routing.repository;

import com.infinit.routing.entity.Port;
import org.springframework.data.repository.CrudRepository;

public interface PortRepository extends CrudRepository<Port, String> {

	Port findByName(String name);

}