package com.infinit.routing.service;

import com.infinit.routing.command.RouteCommand;
import com.infinit.routing.dto.RouteDto;
import com.infinit.routing.entity.Leg;
import com.infinit.routing.entity.Port;
import com.infinit.routing.repository.PortRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Service for routing functions
 * <p>
 * Created by owahlen on 26.04.15.
 */
@Service
public class RouteService {

    private final static Integer DEFAULT_MAX_ROUTE_LENGTH = 4;

    @Autowired
    private PortRepository portRepository;

    /**
     * Find all existing routes from startPort to endPort that do not contain loops and that are no longer
     * than MAX_ROUTE_LENGTH
     *
     * @param command RouteCommand containing startPort and endPort
     * @return Collection of RouteDtos
     */
    public Collection<RouteDto> findRouteDtos(RouteCommand command) {
        Port startPort = portRepository.findByName(command.getStartPort());
        Port endPort = portRepository.findByName(command.getEndPort());
        Integer maxRouteLength = command.getMaxRouteLength() != null ? command.getMaxRouteLength() : DEFAULT_MAX_ROUTE_LENGTH;
        if (startPort == null || endPort == null) {
            StringJoiner joiner = new StringJoiner(" and ");
            if (startPort == null) {
                joiner.add("start port '" + command.getStartPort() + "' not found");
            }
            if (endPort == null) {
                joiner.add("end port '" + command.getEndPort() + "' not found");
            }
            throw new HttpMessageNotReadableException(joiner.toString());
        }
        Collection<List<Leg>> routes = findRoutes(startPort, endPort, maxRouteLength);
        return convertToRouteDtos(routes);
    }

    /**
     * Find all existing routes from startPort to endPort that do not contain loops and that are no longer
     * than maxRouteLength
     *
     * @param startPort      from where to start the route
     * @param endPort        where to end the route
     * @param maxRouteLength maximum length of the route to return
     * @return Collection of routes that are represented by a List of Legs respectively
     */
    public Collection<List<Leg>> findRoutes(Port startPort, Port endPort, Integer maxRouteLength) {
        Collection<List<Leg>> routes = new HashSet<List<Leg>>();
        findRoutes(startPort, endPort, new ArrayList<Leg>(), routes, maxRouteLength);
        return routes;
    }

    /**
     * The actual graph traversal is realized by this recursive method
     *
     * @param startPort      initialized with the startPort and modified throughout the recursion
     * @param endPort        endPort of the route
     * @param route          represented by a list of Legs
     * @param matchingRoutes populated by the recursion to hold the collection of proper routes
     * @param maxDepth       is initialized with the maxRouteLength and reduced in each recursion
     */
    void findRoutes(Port startPort, Port endPort, List<Leg> route, Collection<List<Leg>> matchingRoutes, Integer maxDepth) {
        // check if the current route is one that leads to the endPort
        if (Objects.equals(startPort.getName(), endPort.getName())) {
            matchingRoutes.add(route);
            return;
        }
        // check if the maximum search depth has been reached
        if (maxDepth <= 0) {
            return;
        }
        // iterate through the startPort's legs
        for (Leg leg : startPort.getLegs()) {
            Port newStartPort = leg.getEndPort();
            // use the leg as new startPort if it has not yet been visited
            if (!routeContainsPort(route, newStartPort)) {
                List<Leg> newRoute = new ArrayList<Leg>(route);
                newRoute.add(leg);
                findRoutes(newStartPort, endPort, newRoute, matchingRoutes, maxDepth - 1);
            }
        }
    }

    /**
     * Check if a route contains a leg whose startPort is equal to the port
     *
     * @param route represented by a List of Legs
     * @param port  to check for
     * @return true if and only if the port is one of the startPorts in the List of Legs
     */
    Boolean routeContainsPort(List<Leg> route, Port port) {
        for (Leg leg : route) {
            if (Objects.equals(leg.getStartPort(), port)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Convert the routes consisting of Leg entities into a Dto representation
     *
     * @param routes represented as Collection of List of Legs
     * @return Collection of RouteDtos
     */
    Collection<RouteDto> convertToRouteDtos(Collection<List<Leg>> routes) {
        Collection<RouteDto> routeDtos = new HashSet<RouteDto>();
        for (List<Leg> legs : routes) {
            RouteDto routeDto = new RouteDto(legs);
            routeDtos.add(routeDto);
        }
        return routeDtos;
    }

}
