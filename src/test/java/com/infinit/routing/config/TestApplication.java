package com.infinit.routing.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Test application configuration that allows to override production settings
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan
public class TestApplication extends Application {
}
