package com.infinit.routing.repository;

import com.infinit.routing.config.DevelopmentFixtures;
import com.infinit.routing.entity.Leg;
import com.infinit.routing.entity.Port;
import com.infinit.routing.test.AbstractTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SuppressWarnings("SpringJavaAutowiredMembersInspection")
public class PortRepositoryTest extends AbstractTest {

	@Autowired
	private PortRepository portRepository;

	@Test
	public void testFindPortByName() {
		  // when
		Port hamburg = portRepository.findByName("665535");

		// then
		assertNotNull(hamburg);
		assertEquals(hamburg.getName(), "665535");
		Set<Leg> legs = hamburg.getLegs();
		assertEquals(2, legs.size());
	}

}
