package com.infinit.routing.service;

import com.infinit.routing.command.RouteCommand;
import com.infinit.routing.entity.Port;
import com.infinit.routing.repository.PortRepository;
import com.infinit.routing.test.AbstractTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Test for the RouteService
 * <p>
 * Created by owahlen on 26.04.15.
 */
@SuppressWarnings("SpringJavaAutowiredMembersInspection")
public class RouteServiceTest extends AbstractTest {

    @Autowired
    private PortRepository portRepository;

    @Autowired
    private RouteService routeService;

    @Test
    public void testFindAllRouteDtos() throws FileNotFoundException, UnsupportedEncodingException {

        PrintWriter writer = new PrintWriter("routing-performance.csv", "UTF-8");
        writer.println("START_PORT;END_PORT;DURATION_MILLIS");
        for (Port startPort : portRepository.findAll()) {
            for (Port endPort : portRepository.findAll()) {
                RouteCommand command = new RouteCommand();
                command.setStartPort(startPort.getName());
                command.setEndPort(endPort.getName());
                command.setMaxRouteLength(3);

                Long startMillis = System.currentTimeMillis();
                routeService.findRouteDtos(command);
                Long endMillis = System.currentTimeMillis();
                Long durationMillis = endMillis - startMillis;
                writer.println(startPort.getName() + ";" + endPort.getName() + ";" + durationMillis);
                System.out.println(startPort.getName() + ";" + endPort.getName() + ";" + durationMillis);
            }
        }
        writer.close();
    }
}
